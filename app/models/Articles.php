<?php
/**
 * Created by PhpStorm.
 * User: masha
 * Date: 12.11.18
 * Time: 12:50
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Articles extends Model
{

    protected $table = "articles";
    protected $fillable = array('topic_id', 'count-view', 'article_id', 'ip');
    public $timestamps = false;

    public function load($mas = [])
    {
        $this->topic_id = $mas['topic_id'];
        $this->article_id = $mas['article_id'];
        $this->count_view = $mas['count_view'];
    }


}