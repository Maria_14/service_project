<?php
return [
    '' => [
        'controller' => 'main',
        'action' => 'index',
    ],
	'update' => [
		'controller' => 'main',
		'action' => 'update',
	],
    'fixcount' => [
        'controller' => 'main',
        'action' => 'fixcount',
    ],

];
?>