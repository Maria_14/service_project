<?php
/**
* 
*/
namespace App\Controllers;


use App\Core\Classes\Controller;
use Illuminate\Database\Capsule\Manager as DB;
use App\Models\Articles;
use Pusher\Pusher;


class Main  extends Controller
{
    public function indexAction()
    {
        $this->view->render();
    }
	
	public function updateAction()
	{

        if(empty($_POST['article_id']) || empty($_POST['topic_id'])){
            return json_encode(array('error' => 'Empty param article_id or topic_id'));
        }
        $articleId = $_POST['article_id'];
        $topicId = $_POST['topic_id'];

        $modelArticle = Articles::where('article_id', '=', $articleId)->first();
        if(!empty($modelArticle)) {
            $modelArticle->count_view = 0;
            $modelArticle->save();
        } else {
            $modelArticle = new Articles();
            $modelArticle->load([
                'article_id'=>$articleId,
                'topic_id' => $topicId,
                'count_view' => 0
            ]);
            $modelArticle->save();
        }


        $mas = [
            'article_id' => $articleId,
            'count_view'=> $modelArticle->count_view
        ];

        $topArticle  = DB::table('articles')
            ->select(['article_id', 'count_view'])
            ->where(['topic_id' => $topicId])
            ->where('article_id', '<>', $articleId)
            ->orderBy('count_view', 'DESC')
            ->limit(3)
            ->get();

        $mas[] = $topArticle;
        $this->send($mas);


        echo json_encode(array('success' => true));

	}
	public function fixCountAction()
    {

        if(empty($_POST['article_id']) || empty($_POST['topic_id'])){
            return json_encode(array('error' => 'Empty param article_id or topic_id'));
        }
        $articleId = $_POST['article_id'];
        $topicId = $_POST['topic_id'];

        $modelArticle = Articles::where('article_id', '=', $articleId)->first();
        if(!empty($modelArticle)) {
            $modelArticle->count_view = $modelArticle->count_view + 1;
            $modelArticle->save();
        } else {
            $modelArticle = new Articles();
            $modelArticle->load([
                'article_id'=>$articleId,
                'topic_id' => $topicId,
                'count_view' => 1
            ]);
            $modelArticle->save();
        }


        $mas = [
            'article_id' => $articleId,
                'count_view'=> $modelArticle->count_view
        ];

        $topArticle  = DB::table('articles')
            ->select(['article_id', 'count_view'])
            ->where(['topic_id' => $topicId])
            ->where('article_id', '<>', $articleId)
            ->orderBy('count_view', 'DESC')
            ->limit(3)
            ->get();
        array_push($mas, $topArticle);
        $this->send($mas);


        echo json_encode(array('success' => true));

    }
    public function send($data)
    {
        $options = array(
            'cluster' => 'eu',
            'useTLS' => true
        );
        $pusher = new Pusher(
            '4f4af9d0ae2fe5c08c28',
            '61cb436ac37b711ea648',
            '655098',
            $options
        );

        $pusher->trigger('my-channel', 'my-event', $data);


    }

}