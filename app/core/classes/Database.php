<?php
namespace App\Core\Classes;
use App\Core\Classes\Config;
use PDO;
use Illuminate\Database\Capsule\Manager as Capsule;
class Database {
    function __construct() {
        $config = Config::Load('database');
        $capsule = new Capsule;

        $capsule->addConnection([
            "driver" => "mysql",
            "host" => "mysql",
            "database" => $config["dbname"],
            "username" => $config["user"],
            "password" => $config["pass"],
            "charset" => "utf8",
            "collation" => "utf8_unicode_ci",
            "prefix" => "",
            'options'  => array(PDO::MYSQL_ATTR_LOCAL_INFILE => true),
        ]);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}