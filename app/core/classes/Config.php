<?php

namespace App\Core\Classes;

class Config 
{
	
	public static function Load($name)
	{

		  return include_once __ROOT__.'/config/'.$name.'.php';
	}
}