<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?= $title ?></title>
</head>
<body>
<div class="wrapper">
    <div id="header-wrapper">

    </div>
    <main role="main" >
            <?php require $content; ?>
    </main>
   <script src="/12345/jquery-3.1.1.js"></script>
    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <script src="/12345/main.js"></script>
</div>
</body>
</html>
