var page ={
    init:function () {
        var self = this;
        Pusher.logToConsole = true;
        var pusher = new Pusher('8eb1b2c8d3d1b8a4652f', {
            cluster: 'eu',
            forceTLS: true
        });
        var channel = pusher.subscribe('count-channel');
        channel.bind('my-event', function(data) {

            var mas = {article_id:data.id, topic_id: data.topic_id};

            var url = "/" + data.rout;

           self.ajaxCall(url, mas, function(msg) {
                console.log(msg);
            });
        });
    },
    ajaxCall:function (ajax_url, ajax_data, successCallback) {
        $.ajax({
            type : "POST",
            url : ajax_url,
            dataType : "json",
            data: ajax_data,
            success : function(msg) {
                successCallback(msg);

                if( msg.success ) {
                    successCallback(msg);
                } else {
                    console.log(msg.errormsg);
                }

            },
            error: function(msg) {
                console.log(msg);
            }
        });

    }
}

window.addEventListener("load", function () {
    page.init();
});