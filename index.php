<?php
session_start();
ini_set('memory_limit', '-1');
include './vendor/autoload.php';
use App\Core\Classes\Router;
use App\Core\Classes\Database;

define('__ROOT__', './app');
define("URLROOT", "/");

$dt = new Database();

$rout = new Router('routs');
$rout->run();
?>